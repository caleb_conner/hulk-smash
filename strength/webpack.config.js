module.exports = {
  entry: './src/app.js',
  target: 'node',
  output: {
    path: __dirname + '/bin',
    filename: 'bundle.js'
  },
  module: {
    loaders: [{
      test: /\.js$/,
      exclude: [/node_modules/, /bin/],
      loader: 'babel-loader'
    }]
  }
}

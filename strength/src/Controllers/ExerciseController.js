import Exercise from '../Models/Exercise/Exercise';
import repository from '../Repositories/Repository';

export default class ExerciseController {

  constructor() {
    this.add = this.add.bind(this);
    this.list = this.list.bind(this);
    this.get = this.get.bind(this);
    this.repository = repository();
  }

  list(req, res, next) {
    return res.json({ exercises: this.repository.getExercises().map(e => e.toJSON())});
  };

  add(req, res, next) {
    let exercise = new Exercise(req.body.name, req.body.description);
    this.repository.addExercise(exercise);
    res.json({ exercise:exercise.toJSON() }); 
  };

  get(req, res, next) {
    let exerciseID = Number(req.params.exerciseid);
    let exercise = this.repository.getExercise(exerciseID);
    res.json({exercise:exercise});
  };

}

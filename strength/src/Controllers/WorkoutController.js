import Workout from '../Models/Workout/Workout';
import WorkoutExercise from '../Models/Workout/WorkoutExercise'
import repository from '../Repositories/Repository';

export default class WorkoutController {

  constructor() {
    this.add = this.add.bind(this);
    this.list = this.list.bind(this);
    this.get = this.get.bind(this);
    this.repository = repository();
  }

  list(req, res, next) {
    return res.json({ workouts: this.repository.getWorkouts().map(e => e.toJSON())});
  };

  add(req, res, next) {
    let workout = new Workout(req.body.name, req.body.exercises.map((exercise) => {
      console.log(exercise);
      console.log(!exercise.hasOwnProperty('id') +" "+ !exercise.hasOwnProperty('reps') +" "+ !exercise.hasOwnProperty('sets'));
      if(!exercise.hasOwnProperty('id') || !exercise.hasOwnProperty('reps') || !exercise.hasOwnProperty('sets')) {
        res.status(400).end();
      }

      if(!this.repository.exerciseExists(exercise.id)) {
        res.status(404).end();
      }

      return new WorkoutExercise(exercise.id, exercise.sets, exercise.reps);

    }));
    
    this.repository.addWorkout(workout);
    res.json({ workout:workout.toJSON() }); 
  };

  get(req, res, next) {
    let workoutID = Number(req.params.workoutid);
    let workout = this.repository.getWorkout(workoutID);
    res.json({workout:workout});
  };

}

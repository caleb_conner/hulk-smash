export default class InSessionRepository {
  
  constructor() {
    this.exercises = [];
    this.workouts = [];
    this.workoutExercises = [];

    this.addExercise = this.addExercise.bind(this);
    this.getExercises = this.getExercises.bind(this);
    this.getExercise = this.getExercise.bind(this);
    this.exerciseExists = this.exerciseExists.bind(this);

    this.addWorkout = this.addWorkout.bind(this);
    this.getWorkouts = this.getWorkouts.bind(this);
  };

  addExercise(elem) {
    this.exercises.push(elem);
  }

  getExercises() {
    return this.exercises;
  }

  getExercise(exerciseID) {
    return this.exercises.filter(e => e.getID() === exerciseID)[0];
  }

  exerciseExists(exerciseId) {
    console.log(exerciseId);
    console.log(this.exercises);
    let exer = this.getExercise(exerciseId);

    return exer !== undefined && exer !== null;
  }

  addWorkout(elem) {
    this.workouts.push(elem);
  }

  getWorkouts() {
    return this.workouts;
  }

  addWorkoutExercise(workoutExercise) {
    this.workoutExercises.push(workoutExercise);
  }
};

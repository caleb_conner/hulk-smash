import exerciseIDController from './ExerciseIDController';

export default class Exercise {

  constructor(name, description) {

    this.name = name;
    this.description = description;
    this.id = exerciseIDController.next();

  }

  getID() {
    return this.id;
  }

  toJSON() {
    return { exerciseid:this.id, name:this.name, description:this.description };
  }

  toString() {

    return `${this.name}: ${this.description}`;

  }

};

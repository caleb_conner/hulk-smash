import workoutIDController from './WorkoutIDController';

export default class Workout {

	constructor (name, workoutExercises) {
    this.id = workoutIDController.next();
		this.name = name;
    this.workoutExercises = workoutExercises || [];
	};

  getExercises() {
    return this.workoutExercises || [];
  };

  toJSON() {
    return { 
      workoutid:this.id,
      name:this.name,
      exercises:this.workoutExercises.map(we => we.toJSON())
    };
  }

	toString() {
		return `Name: ${this.name}\n` +
           `Exercises: ${this.workoutExercises.map(e => `\n  ${e}`)}`;
	};

};

export default class WorkoutExercise {

  constructor(exerciseID, sets, reps) {
    this.exerciseID = exerciseID;
    this.sets = sets;
    this.reps = reps;
  }

  toJSON() {
    return {
      exerciseid: this.exerciseID,
      sets: this.sets,
      reps: this.reps
    }
  }

  toString() {
    return `${this.exercise.name} ${this.sets}x${this.reps}`;
  }

};

import express from 'express';
let app = express();
import bodyParser from 'body-parser';

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

let port = process.env.PORT || 3000;

//Set up repo
import repository from './Repositories/Repository';
repository(0);

// Set up routes
import router from './Routes/Router';

router(app);

app.listen(port);

console.log(`Listening on port: ${port}`);

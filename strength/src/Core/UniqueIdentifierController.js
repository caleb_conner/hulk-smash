export default class UniqueIdentifierController {
  
  constructor(seed) {
    this.nextID = seed;
  }

  next() {
    let next = this.nextID;
    this.nextID += 1;
    return next;
  }
};

import exerciseRoutes from './ExerciseRoutes';
import workoutRoutes from './WorkoutRoutes';

export default (app) => {
  exerciseRoutes(app);
  workoutRoutes(app);
};

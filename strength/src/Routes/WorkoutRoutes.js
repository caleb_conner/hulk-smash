import WorkoutController from '../Controllers/WorkoutController';

export default (app) => {

  let workoutController = new WorkoutController();

  app.route('/workout')
    .post(workoutController.add)
    .get(workoutController.list);

  app.route('/workout/:workoutid')
    .get(workoutController.get);
};

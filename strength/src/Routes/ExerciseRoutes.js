import ExerciseController from '../Controllers/ExerciseController';

export default (app) => {

  let exerciseController = new ExerciseController();

  app.route('/exercise')
    .post(exerciseController.add)
    .get(exerciseController.list);

  app.route('/exercise/:exerciseid')
    .get(exerciseController.get);
};

module.exports = {
  entry: '.src/index.js',
  target: 'node',
  output: {
    path: __dirname + '/public',
    filename: 'bundle.js'
  },
  module: {
    loaders: [{
      test:/\.js$/,
      exclude: [/node_modules/, /public/],
      loader: 'babel-loader'
      options: {
        presets: ['es2015', 'react']
      }
    }]
  }
}
